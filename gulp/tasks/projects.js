var gulp = require('gulp');
var config = require('../config').projects;

gulp.task('projects', function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
