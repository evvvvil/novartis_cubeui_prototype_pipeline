// Creates the scene Lighting.
// Give it some shadows too, while your here.

define(function (require) {

	var Three  = require('Three');

	/*
	 * Pass in the Canvas Scene, Cubes array, and the Texture;
	 */
	var Lights = function(scene, lights){

		var scope = this;

		// Direnctional Lights
		var directionalLight = new THREE.DirectionalLight( 0xc3c3c3, 0.5 );
		directionalLight.position.set( 500, 1000, 500 );
		directionalLight.color.setHSL( 0.6, 1, 0.95 );

		directionalLight.position.multiplyScalar( 500 );

		directionalLight.castShadow = true;
		// directionalLight.shadowCameraVisible = true;

		directionalLight.shadowMapWidth = 2048;
		directionalLight.shadowMapHeight = 2048;

		directionalLight.shadowCameraNear = 200;
		directionalLight.shadowCameraFar = 1500;

		directionalLight.shadowCameraLeft = -500;
		directionalLight.shadowCameraRight = 500;
		directionalLight.shadowCameraTop = 500;
		directionalLight.shadowCameraBottom = -500;

		directionalLight.shadowBias = -0.005;
		directionalLight.shadowDarkness = 0.35;

		scene.add( directionalLight );


		var directionalLight2 = new THREE.DirectionalLight( 0xc3c3c3, 0.5 );
		directionalLight2.position.set( 3000, -0.5, 1 );
		directionalLight2.color.setHSL( 0.6, 1, 0.95 );

		directionalLight2.position.multiplyScalar( 500 );

		directionalLight2.castShadow = true;
		// directionalLight.shadowCameraVisible = true;

		directionalLight2.shadowMapWidth = 2048;
		directionalLight2.shadowMapHeight = 2048;

		directionalLight2.shadowCameraNear = 200;
		directionalLight2.shadowCameraFar = 1500;

		directionalLight2.shadowCameraLeft = -500;
		directionalLight2.shadowCameraRight = 500;
		directionalLight2.shadowCameraTop = 500;
		directionalLight2.shadowCameraBottom = -500;

		directionalLight2.shadowBias = -0.005;
		directionalLight2.shadowDarkness = 0.35;

		// scene.add( directionalLight2 );

		// Ambient Lighting.
		var ambient = new THREE.AmbientLight( 0x444444 );
		scene.add( ambient );


		// Spot Lights.
		light = new THREE.SpotLight( 0xc3c3c3, 0.8, 0, Math.PI / 2, 0 );
		light.position.set( 900, 1000, 1000 );
		light.target.position.set( 500, 200, 400 );

		light.castShadow = true;

		light.shadowCameraNear = 1200;
		light.shadowCameraFar = 2500;
		light.shadowCameraFov = 50;

		// light.shadowCameraVisible = true;
		light.shadowBias = 0.0001;
		light.shadowDarkness = 0.2;
		light.shadowMapWidth = 2048;
		light.shadowMapHeight = 2048;

		scene.add( light );

		// Add to Collection
		lights.push(light);

		//
		var light2 = new THREE.SpotLight( 0xc3c3c3, 0.7, 0, Math.PI / 2, 0 );
		light2.position.set( 2900, 1000, 1000 );
		light2.target.position.set( 2400, 200, 400 );


		light2.castShadow = true;

		light2.shadowCameraNear = 1200;
		light2.shadowCameraFar = 2500;
		light2.shadowCameraFov = 50;

		// light.shadowCameraVisible = true;
		light2.shadowBias = 0.0001;
		light2.shadowDarkness = 0.2;
		light2.shadowMapWidth = 4096;
		light2.shadowMapHeight = 4096;

		// scene.add( light2 );


	}

	return Lights

});
