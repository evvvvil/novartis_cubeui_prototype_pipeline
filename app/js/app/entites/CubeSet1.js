// Creates a face array of Materials for the cube.

define(function (require) {

	var Three  = require('Three'),
			BlankMaterialTextures   = require('app/entites/Materials/CubeBlank'),
			MaterialTextures1   = require('app/entites/Materials/Sect1_title'),
			MaterialTextures2   = require('app/entites/Materials/Sect1_1'),
			MaterialTextures3   = require('app/entites/Materials/Sect1_2');

	/*
	 * Pass in the Canvas Scene, Cubes array, and the Texture;
	 */
	var CubeSet = function(scene, cubes, offx, offy){

		var scope = this;
		this.cubegap = 1;
		this.cubesgroup = 12; // Total amount of cubes
		this.cubewidth = 265;
		this.offset = {
			x:offx || 0,
			y:offy || 0
		}
		// Cube
		var geometry = new THREE.BoxGeometry( scope.cubewidth, scope.cubewidth, scope.cubewidth );
		var materials;

		for ( var i = 0; i < geometry.faces.length; i += 2 ) {
			var hex = Math.random() * 0xffffff;
			geometry.faces[ i ].color.setHex( hex );
			geometry.faces[ i + 1 ].color.setHex( hex );
		}

		var cubematt = new BlankMaterialTextures();

		// Create all the cubes elements
		var row = 0;
		for (i = 0; i < scope.cubesgroup; i++) {

				// Select the Material for the cubes.
				switch (i){

					case 2:
						var cubetextured = new MaterialTextures3();
						materials = cubetextured.material;
					break;

					case 4:
						var cubetextured = new MaterialTextures1();
						materials = cubetextured.material;
					break;

					case 6:
						var cubetextured = new MaterialTextures2();
						materials = cubetextured.material;
					break;

					default:
						// Select the material.
						materials = cubematt.material;
					break;
				}


				// var material = new THREE.MeshBasicMaterial( { vertexColors: THREE.FaceColors, overdraw: 0.5 } );
				var material = new THREE.MeshFaceMaterial(materials);
				var cube = new THREE.Mesh( geometry, material );
				cube.overdraw = true;
				cube.position.y = 0;
				cube.flipped = false;

				/*
				* Layout the cubes in a Grid.
				* Needs some tidying up.
				*/
				if(i === 4){
					row = 0;
				}
				if(i === 8){
					row = 0;
				}
				if((i >=4)&&(i<8)){
					cube.position.y = scope.cubewidth + scope.cubegap;
				}
				else if(i >=7){
					// 2nd Row in grid
					cube.position.y = (scope.cubewidth*2) + (scope.cubegap*2);
				}
				cube.position.x = scope.cubewidth * row + (scope.cubegap*row) ;

				// add the offset for the grid
				cube.position.x = cube.position.x + scope.offset.x;
				cube.position.y = cube.position.y + scope.offset.y;

				//Increment row count
				row += 1;
				cube.rotation.y = Math.random() * 0.1 + 1.6;
				cube.name = i;
				cube.castShadow = true;
				cube.receiveShadow = true;

				// Add to array and scene.
				cubes.push(cube);
				// Add to scene.
				scene.add(cube);
		}

	}

	return CubeSet

});
