// Creates a face array of Materials for the cube.

define(function (require) {

	var Three     = require('Three');

	var Material = function(){

		var scope = this;

		// Bump texture
		var mapHeight = THREE.ImageUtils.loadTexture( "imgs/cubes/grey_bump.png" );
		mapHeight.anisotropy = 4;
		mapHeight.repeat.set( 0.998, 0.998 );
		mapHeight.offset.set( 0.001, 0.001 )
		mapHeight.wrapS = mapHeight.wrapT = THREE.RepeatWrapping;
		mapHeight.format = THREE.RGBFormat;

		var bumpscale = 1;

		this.material = [
				new THREE.MeshPhongMaterial({
					// color: 0x926033,
					polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1,
					shading: THREE.FlatShading,
					transparent: false,
					bumpMap: mapHeight,
					bumpScale: bumpscale,
					specular: 0x333333, shininess: 5,
					metal: false,
					map: THREE.ImageUtils.loadTexture( 'imgs/cubes/cube1_2.png' )
				}),
				new THREE.MeshPhongMaterial({
					// color: 0x926033,
					polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1,
					shading: THREE.FlatShading,
					transparent: false,
					bumpMap: mapHeight,
					bumpScale: bumpscale,
					specular: 0x333333, shininess: 5,
					metal: false,
					map: THREE.ImageUtils.loadTexture( 'imgs/cubes/grey.png' )
				}),

				new THREE.MeshPhongMaterial( {
					// color: 0xffff00,
					polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1,
					shading: THREE.FlatShading,
					transparent: false,
					bumpMap: mapHeight,
					bumpScale: bumpscale,
					specular: 0x333333, shininess: 5,
					metal: false,
					map: THREE.ImageUtils.loadTexture( 'imgs/cubes/grey.png' )
				} ),

				new THREE.MeshPhongMaterial({
					// color: 0x926033,
					polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1,
					shading: THREE.FlatShading,
					transparent: false,
					bumpMap: mapHeight,
					bumpScale: bumpscale,
					specular: 0x333333, shininess: 5,
					metal: false,
					map: THREE.ImageUtils.loadTexture( 'imgs/cubes/grey.png' )
				}),

				new THREE.MeshPhongMaterial( {
						// color: 0xff0000,
						polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1,
						shading: THREE.FlatShading,
						transparent: true,
						opacity: 1.0,
						bumpMap: mapHeight,
						bumpScale: bumpscale,
						specular: 0x333333, shininess: 5,
						metal: false,
						map: THREE.ImageUtils.loadTexture( 'imgs/cubes/cube7_3.png' )
				} ),

				new THREE.MeshPhongMaterial({
					// color: 0x926033,
					polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1,
					shading: THREE.FlatShading,
					transparent: false,
					bumpMap: mapHeight,
					bumpScale: bumpscale,
					specular: 0x333333, shininess: 5,
					metal: false,
					map: THREE.ImageUtils.loadTexture( 'imgs/cubes/grey.png' )
				} ),
		];


	}

	return Material

});
