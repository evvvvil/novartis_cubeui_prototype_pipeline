/*
 * *** *** *** *** ***
 * *** *** *** *** ***
 * *** CHESH PAWEL ***

      ( . )  ( . )

      ------------

 * The bulk of the THREEJS interactivity and rendering takes place here.
 * The wall is made up of 10 cubes sets. Each cubeset has 12 cubes.
 * Upon click the cube flips (sets the flipped value), and will flip back on additional press.
 * The Cubesets and Cube Materials are in the Entites modules.
 */

// coded [ Josh : info@designbuildplay.co.uk ]

// THE HOME VIEW :::::::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        TweenMax           = require('tweenmax'),
        Three              = require('Three'),
        Projector          = require('Projector'),
        Detector           = require('Detector'),
        SectionCollection  = require('app/collections/Projects'),
        CubeSet            = require('app/entites/CubeSet'),
        CubeSet1           = require('app/entites/CubeSet1'),
        CubeSet2           = require('app/entites/CubeSet2'),
        CubeSet3           = require('app/entites/CubeSet3'),
        CubeSet4           = require('app/entites/CubeSet4'),
        CubeSet5           = require('app/entites/CubeSet5'),
        CubeSet6           = require('app/entites/CubeSet6'),
        CubeSet7           = require('app/entites/CubeSet7'),
        CubeSet8           = require('app/entites/CubeSet8'),
        CubeSet9           = require('app/entites/CubeSet9'),
        Lights             = require('app/entites/Lights'),
        template           = require("./text!../templates/home.html");

    // CONTENT ::
    var scope;

    var HomeView = Backbone.View.extend({

        tagName:'div',
        id:"slides",
        el:'#viewport',  //selects element rendering to
        template: _.template( template ), //selects the template with given name

        events: {
           'click .shuffle': 'spinAll',
           'click .move':'moveGridCamera',
           'click .home': 'moveGridHome',
          //  'keydown': 'keyAction'
        },

        initialize:function () {
              scope = this;
              this.render();
              console.log("VIEWING Home ");
              $(document).bind('keydown', this.keyAction);
        },

        render:function () {

            this.$el.html(this.template());

            this.container;
            this.camera, this.scene, this.renderer, this.projector;
            this.cameratween;
            this.lights = [];
            this.cubes1 = [];
            this.cubes2 = [];
            this.cubes3 = [];
            this.cubes4 = [];
            this.cubes5 = [];
            this.cubes6 = [];
            this.cubes7 = [];
            this.cubes8 = [];
            this.cubes9 = [];
            this.cubes10 = [];

            this.cubewidth = 265;
            this.cameraZoom = 550; // Default zoom is 550.
            this.cubegap = 1;
            this.cubesgroup = 12; // Total amount of cubes
            this.mouseX = 0;
            this.mouseXOnMouseDown = 0;
            this.w = 1920;
            this.h = 1080;
            this.wallstate = 1;
            this.onHome = true;
            this.cubeRoations = [0, -1.58, -3.16, -4.74];
            this.panelVisible = false; // Visibilty of the Info Panel.

            // Call the main canvas methods.
            this.init();
            this.animate();

            scope.allCubes = [
              scope.cubes1,
              scope.cubes2,
              scope.cubes3,
              scope.cubes4,
              scope.cubes5,
              scope.cubes6,
              scope.cubes7,
              scope.cubes8,
              scope.cubes9,
              scope.cubes10,
            ]

          // Fade in container.
          TweenMax.to(scope.$el.find('.container'), 1, {alpha:1});

          // Listen for changes of the Panel Open.
          SectionCollection.models[0].on('change:subopen', function(model, updatedValue) {
            console.log('subopen changed  ', updatedValue);
            if(updatedValue === true){
              // Set value to true, this will disable the Wall cube Interactity.
              scope.panelVisible = true;
            }else{
              scope.panelVisible = false;

              if(  !SectionCollection.models[0].get('returnHome') ){
                // Move camera back to previous wall position
                scope.tweenCameraView(scope.wallstate);
              }else{
                // Move camera back home.
                scope.moveGridHome();
              }
            }
          });


          return this;
        },

        init:function(){

          scope.canvas = scope.$el.find('.scopecanvas');

          scope.camera = new THREE.PerspectiveCamera( 70, scope.w / scope.h, 1, 2000 );
          scope.camera.position.x = 390;
          scope.camera.position.y = 300;
          scope.camera.position.z = scope.cameraZoom;
          scope.scene = new THREE.Scene();
          scope.scene.fog = new THREE.Fog( 0xffffff, 1000, 10000 );

          // Cubes
          // Creates the cubes and the Materials.
          var offset = (scope.cubewidth * 4) + (scope.cubegap * 4);

          // Create the original cube set
          var cubeset1 = new CubeSet1(scope.scene, scope.cubes1);

          // Create the surrounding cubesets.
          var cubeset2 = new CubeSet2(scope.scene, scope.cubes2, 0, offset-265);
          var cubeset3 = new CubeSet3(scope.scene, scope.cubes3, offset, offset-265);
          var cubeset4 = new CubeSet4(scope.scene, scope.cubes4, offset, 0);
          var cubeset5 = new CubeSet5(scope.scene, scope.cubes5, (offset*2), offset-265 );
          var cubeset6 = new CubeSet6(scope.scene, scope.cubes6, (offset*3), offset-265 );
          var cubeset7 = new CubeSet7(scope.scene, scope.cubes7, (offset*2), 0 );
          var cubeset8 = new CubeSet8(scope.scene, scope.cubes8, (offset*3), 0 );
          var cubeset9 = new CubeSet9(scope.scene, scope.cubes9, (offset*4), 0 );
          var cubeset10 = new CubeSet(scope.scene, scope.cubes10, (offset*4), offset-265 );

          // LIGHTS
          var lights = new Lights(scope.scene, scope.lights);

          scope.projector = new THREE.Projector();

          // Pick the renderer.
          scope.renderer = new THREE.WebGLRenderer( { antialias: true,  alpha: true } );
          scope.renderer.setClearColor( 0xffffff, 0 );

          // scope.renderer.setClearColor( scope.scene.fog.color );
          scope.renderer.setPixelRatio( window.devicePixelRatio );
          scope.renderer.setSize( scope.w, scope.h );
          scope.renderer.shadowMapEnabled = true;
          scope.renderer.shadowMapType = THREE.PCFSoftShadowMap;

          scope.renderer.gammaInput = true;
          scope.renderer.gammaOutput = true;

          // Add to the DOM
          $(scope.canvas).append( scope.renderer.domElement );

          // Call rotation.
          scope.rotateCubesIn()

          // Call the Shimmer
          scope.shimmerCubes();

          // Create the listners.
          document.addEventListener( 'mousemove', scope.onDocumentMouseMove, false );
          document.addEventListener( 'mousedown', scope.onDocumentMouseDown, false );
          document.addEventListener( 'touchstart', scope.onDocumentTouchStart, false );
        },

        rotateCubesIn:function(){
          for (var i = 0; i < scope.cubesgroup; i++) {
            TweenMax.to(scope.cubes1[i].rotation, 1, {
              y:Math.random() * 0.1 + scope.cubeRoations[0],
              ease:Power2.easeOut,
              delay:0.08*i,
            });

          }
        },

        /*
         *  Spin back all cubes and reset there Flipped attribute to the default.
         */
        spinAll:function(){
          // console.log("spin all back.")
          for (var i = 0; i < scope.cubesgroup; i++) {

            for (var c = 0; c < scope.allCubes.length; c++) {
              var cubecollection = scope.allCubes[c];
              var  cube = cubecollection[i];

              TweenMax.set(cube.rotation, {
                y:Math.random() * 0.1 + 1.6
              });

              // Reset Flip
              cube.flipped = false;
              // console.log(cube);
              TweenMax.to(cube.rotation, 1, {
                y:Math.random() * 0.1 + scope.cubeRoations[0],
                ease:Power2.easeOut,
                delay:0.08*i,
              });
            }

          }
        },

        moveGridCamera:function(x, y, reset){
          if(reset !== false){
            TweenMax.to(scope.camera.position, 1, {
              x:Math.random() * 700 + 400,
              y:Math.random() * 700 + 400,
              z:Math.random() * 500 + 400,
              ease:Power2.easeOut
            });
          }
        },

        /*
         * Resets the camera back to the Home position.
         */
        moveGridHome:function(){
          TweenMax.to(scope.camera.position, 1.6, {
            x:390,
            y:300,
            z:scope.cameraZoom,
            ease:Power2.easeOut
          });
          // Reset cubes.
          scope.spinAll();
          // Reset Wall View State
          scope.wallstate = 1;
          scope.onHome = true;
          // Reset Home Model value
          SectionCollection.models[0].set('returnHome', false);
        },

        onDocumentMouseDown:function(event) {
    				event.preventDefault();

            // Perform the 3d Hit check if overlay panel is NOT open.
            if(!scope.panelVisible){

              var vector = new THREE.Vector3( ( event.clientX /scope.w ) * 2 - 1, - ( event.clientY / scope.h ) * 2 + 1, 0.5 );
      				scope.projector.unprojectVector( vector, scope.camera );
      				var raycaster = new THREE.Raycaster( scope.camera.position, vector.sub( scope.camera.position ).normalize() );

              // Loop through all Cubes containers.
              // If on the active wallview state, then add the raycasting interactivity.
              for (var i = 0; i < 10; i++) {
                if(i === (scope.wallstate-1)){
                  var intersects = raycaster.intersectObjects( scope.allCubes[i] );
                  scope.flipCubeFromArray(intersects, scope.allCubes[i]);
                }
              }
            }

    		},

        /*
         * The cube array are RayCastered to check if they are hit,
         * Then calls this method to spin the relebent cube hit
         */
        flipCubeFromArray:function(intersector, cubes){
          if(intersector[0]){
            var currentCube = intersector[0].object.name;
            if(intersector[0].object.flipped == false){
              intersector[0].object.flipped = true;
              var spinAmount = Math.random() * 0.1 + scope.cubeRoations[1];
            }else{
              intersector[0].object.flipped = false;
              var spinAmount = Math.random() * 0.1 + scope.cubeRoations[0];
            }

            TweenMax.to(cubes[currentCube].rotation, 0.7, {
              y:spinAmount,
              ease:Power2.easeOut,
              onCompleteParams:[cubes[currentCube]],
              onComplete:scope.changeWallView
            });
          }
        },

        onDocumentTouchStart:function(event) {

    				if ( event.touches.length === 1 ) {

    					event.preventDefault();

              event.clientX = event.touches[ 0 ].pageX;
              event.clientY = event.touches[ 0 ].pageY;

              // scope.checkCubeHover(event);
              scope.onDocumentMouseDown(event);
    				}
    		},

        /*
         * Handles moving the camera around the Grid depending on the answer chosen.
         */
        changeWallView:function(obj){
          console.log('selected ', scope.wallstate, obj.name);

          // Update the Wall Model
          SectionCollection.models[0].set('wallcontent', { wall:scope.wallstate });

          switch(scope.wallstate){
            case 1:
              if(obj.name == 6){
                scope.tweenCameraView(2);
              }
            break;

            case 2:
              if(obj.name == 5){
                scope.tweenCameraView(3);
              }else if(obj.name == 6){
                scope.tweenCameraView(4);
              }
            break;

            case 3:
              if(obj.name == 5){
                scope.tweenCameraView(5);
              }else if(obj.name == 6){
                scope.tweenCameraView(6);
              }
            break;

            case 4:
              if(obj.name == 5){
                scope.tweenCameraView(7);
              }
              else if(obj.name == 6){
                scope.tweenCameraView(8);
              }
              else if(obj.name == 7){
                scope.tweenCameraView(9);
              }
            break;

            case 5:
              if(obj.name == 5){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:1
                });
              }
              else if(obj.name == 6){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:2
                });
              }
            break;

            case 6:
              if(obj.name == 5){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:1
                });
              }
            break;

            case 7:
              if(obj.name == 4){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:1
                });
              }
              else if(obj.name == 5){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:2
                });
              }
              else if(obj.name == 6){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:3
                });
              }
              else if(obj.name == 7){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:4
                });
              }
            break;


            case 8:
              if(obj.name == 2){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:1
                });
              }
              else if(obj.name == 4){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:2
                });
              }
              else if(obj.name == 5){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:3
                });
              }
              else if(obj.name == 6){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:4
                });
              }
              else if(obj.name == 7){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:5
                });
              }
            break;

            case 9:
              if(obj.name == 5){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:1
                });
              }
              else if(obj.name == 6){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:2
                });
              }
              else if(obj.name == 7){
                scope.tweenCameraView(10);
                // Update the Wall Model
                SectionCollection.models[0].set('wallcontent', {
                  wall:scope.wallstate,
                  cube:3
                });
              }
            break;

          }

          // Resets to home if lower right cube is hit throughout the app.
          if(obj.name == 3){
            scope.tweenCameraView(1);
          }else if(obj.name == 8){
            // Handles the back button, is always top Left cube.
            scope.moveWallBack();
          }

          // console.log(SectionCollection.models[0].get('wallcontent'))
        },

        /*
         * Handle keyboard presses, used to skip in debug.
         */
        keyAction: function(e) {
            var code = e.keyCode || e.which;
            console.log("key ", code);

            //Load the Panel
            // Handle keyPress's from 1-9-0
            switch(code){
              case 80:
                  // Opens the panel when press 'P'.
                  scope.openInfoPanel();
              break;

              case 49:
                scope.tweenCameraView(1);
              break;
              case 50:
                scope.tweenCameraView(2);
              break;
              case 51:
                scope.tweenCameraView(3);
              break;
              case 52:
                scope.tweenCameraView(4);
              break;
              case 53:
                scope.tweenCameraView(5);
              break;
              case 54:
                scope.tweenCameraView(6);
              break;
              case 55:
                scope.tweenCameraView(7);
              break;
              case 56:
                scope.tweenCameraView(8);
              break;
              case 57:
                scope.tweenCameraView(9);
              break;
              case 48:
                scope.tweenCameraView(10);
              break;
            }
        },

        /*
         *  Handles Tweening and moving the camera around the Wall Scene.
         */
        tweenCameraView: function(view) {

            scope.onHome = false;

            // Spin face back to default.
            scope.spinAll();

            //kill the movment first.
            TweenMax.killTweensOf(scope.cameratween);

            switch(view){
              case 1:
                scope.moveGridHome();
              break;
              case 2:
                scope.wallstate = 2;
                scope.cameratween = TweenMax.to(scope.camera.position, 0.7, { y:1080, x:390, z:scope.cameraZoom, ease:Power2.easeOut });
                // scope.tweenCameraZ();
              break;
              case 3:
                scope.wallstate = 3;
                scope.cameratween = TweenMax.to(scope.camera.position, 1, { y:1080, x:1450, z:scope.cameraZoom, ease:Power2.easeOut });
                scope.tweenCameraZ();
              break;
              case 4:
                scope.wallstate = 4;
                scope.cameratween = TweenMax.to(scope.camera.position, 0.7, { y:300, x:1450, z:scope.cameraZoom, ease:Power2.easeOut });
                scope.tweenCameraZ();
              break;
              case 5:
                scope.wallstate = 5;
                scope.cameratween = TweenMax.to(scope.camera.position, 1, { y:1080, x:2520, z:scope.cameraZoom, ease:Power2.easeOut });
                scope.tweenCameraZ();
              break;
              case 6:
                scope.wallstate = 6;
                scope.cameratween = TweenMax.to(scope.camera.position, 1, { y:1080, x:3590, z:scope.cameraZoom, ease:Power2.easeOut });
                scope.tweenCameraZ();
              break;
              case 7:
                scope.wallstate = 7;
                scope.cameratween = TweenMax.to(scope.camera.position, 0.7, { y:300,  x:2520,z:scope.cameraZoom, ease:Power2.easeOut });
                scope.tweenCameraZ();
              break;
              case 8:
                scope.wallstate = 8;
                scope.cameratween = TweenMax.to(scope.camera.position, 0.7, { y:240, x:3590, z:scope.cameraZoom, ease:Power2.easeOut });
                scope.tweenCameraZ();
              break;
              case 9:
                scope.wallstate = 9;
                scope.cameratween = TweenMax.to(scope.camera.position, 0.7, { y:300, x:4660, z:scope.cameraZoom,  ease:Power2.easeOut });
                scope.tweenCameraZ();
              break;
              case 10:
                // scope.wallstate = 10; // Dont set the last wall state, then it can return the camera back to previous.

                scope.cameratween = TweenMax.to(scope.camera.position, 1.7, {
                        y:930,
                        x:4235,
                        z:400,
                        ease:Power2.easeOut,
                        onComplete:scope.openInfoPanel
                    });
              break;
            }
        },

        /*
         * Pans the camera back a little bit on the Z position, then back into position.
         * Not really needed, but I felt like doing it. SHAZAAAM!
         */
        tweenCameraZ:function(){
          var delayBack = 0.7;
          scope.cameratween = TweenMax.to(scope.camera.position, 0.7, { z:650 });
          scope.cameratween = TweenMax.to(scope.camera.position, 0.7, { z:scope.cameraZoom, delay:delayBack });
        },

        /*
         * Handles moving the camera back one on the wall when Back cube hit.
         */
        moveWallBack:function(){
          // console.log('GO HOME YOUR DRUNK', scope.wallstate);
          switch(scope.wallstate){

            case 2:
              scope.tweenCameraView(1);
            break;

            case 3:
              scope.tweenCameraView(2);
            break;

            case 4:
              scope.tweenCameraView(2);
            break;

            case 5:
              scope.tweenCameraView(3);
            break;

            case 6:
              scope.tweenCameraView(3);
            break;

            case 7:
              scope.tweenCameraView(4);
            break;

            case 8:
              scope.tweenCameraView(4);
            break;

            case 9:
              scope.tweenCameraView(4);
            break;
          }
        },

        /*
         * Animated slightly some of the cubes to entise people into use it.
         */
        shimmerCubes:function(){
          var flip = 0.1;
          var time = 1;
          scope.shimmer = new TimelineMax({ repeat:-1, yoyo:true });
              scope.shimmer.fromTo(scope.cubes1[0].rotation, time, {y:scope.cubes1[0].rotation.y }, {y:flip}, Math.random() * 0.5);
              scope.shimmer.fromTo(scope.cubes1[1].rotation, time, {y:scope.cubes1[1].rotation.y }, {y:flip}, Math.random() * 0.7);
              scope.shimmer.fromTo(scope.cubes1[5].rotation, time, {y:scope.cubes1[5].rotation.y }, {y:flip}, Math.random() * 0.2);
              scope.shimmer.fromTo(scope.cubes1[8].rotation, time, {y:scope.cubes1[8].rotation.y }, {y:flip});
              scope.shimmer.fromTo(scope.cubes1[9].rotation, time, {y:scope.cubes1[9].rotation.y }, {y:flip}, Math.random() * 0.5);
              scope.shimmer.fromTo(scope.cubes1[10].rotation, time, {y:scope.cubes1[10].rotation.y }, {y:flip}, Math.random() * 0.5);
              scope.shimmer.fromTo(scope.cubes1[11].rotation, time, {y:scope.cubes1[11].rotation.y }, {y:flip}, Math.random() * 0.8);
              scope.shimmer.fromTo(scope.cubes1[7].rotation, time, {y:scope.cubes1[7].rotation.y }, {y:flip});
              scope.shimmer.fromTo(scope.cubes1[3].rotation, time, {y:scope.cubes1[3].rotation.y }, {y:flip}, Math.random() * 0.5);

          // scope.shimmer.timeScale(0.5);
          scope.shimmer.play();
        },

        /*
         * Three Animation frame
         */
        animate:function() {
          requestAnimationFrame( scope.animate );
          scope.renderCanvas();

          //Make Spotlight follow the camera position
          scope.lights[0].position.x = scope.camera.position.x + 600;
          scope.lights[0].position.y = scope.camera.position.y + 700;
          scope.lights[0].position.z = scope.camera.position.z + 700;

          scope.lights[0].target.position.x = scope.camera.position.x + 300;
          scope.lights[0].target.position.y = scope.camera.position.y - 50;
          scope.lights[0].target.position.z = scope.camera.position.z + 450;
        },

        /*
         * Three renderer
         */
        renderCanvas:function() {
          scope.renderer.clear();
          scope.renderer.render( scope.scene, scope.camera );
        },

        /*
         * Load the sub view info panel
         */
        openInfoPanel:function(){

          var Router = require('router'),
          appRouter = new Router();
          appRouter.navigate('panelui', {trigger:true }); // TRIGGERS THE PAGE FROM ROUTER
        },

        /*
         * Clean hanging events of the view on change
         */
        dispose:function(){
          scope.active = false;
        }

    });

    /*
     * Our module now returns our view
     */
    return HomeView;

});
