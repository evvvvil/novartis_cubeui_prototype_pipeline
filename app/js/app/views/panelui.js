// THE UI VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        TweenMax           = require('tweenmax'),
        SectionCollection  = require('app/collections/Projects'),
        template           = require("text!../../../templates/panelui.html");

    // CONTENT :::::::::::::::::::::::::::::::::::
    var scope;

    var PanelUI = Backbone.View.extend({

        tagName:'div',
        id:"slides",
        el:'#viewportUI',  //selects element rendering to
        template: _.template( template ), //selects the template with given name

        events: {
             'click .btn_home': 'homeClick',
             'click .btn_back': 'backClick',
             'click .btn_info': 'infoClick',

             // Add touch events otherwise the touchscreen not picking up innerview clicks.
             'touchstart .btn_home': 'homeClick',
             'touchstart .btn_back': 'backClick',
             'touchstart .btn_info': 'infoClick',
        },


        initialize:function () {
              scope = this;
              this.active = true;

              // Retreive active content.
              this.wallcontent = SectionCollection.models[0].get('wallcontent');
              console.log("VIEWING info Panel ", scope.wallcontent);

              this.render();
        },

        render:function () {

            this.$el.html(this.template());

            TweenMax.to(scope.$el.find('.container'), 1, {alpha:1});

            // Set Panel model.
            SectionCollection.models[0].set('subopen', true);

            // Update the box color from the model wall.
            switch(scope.wallcontent.wall){
              case 5:
                $(scope.$el.find('.boxcolor')).addClass('box_brown')
              break;
              case 6:
                $(scope.$el.find('.boxcolor')).addClass('box_red')
              break;
              case 7:
                $(scope.$el.find('.boxcolor')).addClass('box_brown')
              break;
              case 8:
                $(scope.$el.find('.boxcolor')).addClass('box_orange')
              break;
              case 9:
                $(scope.$el.find('.boxcolor')).addClass('box_red')
              break;
            }

            // Load the content
            scope.loadPanelContent();

            return this;
        },


        homeClick:function(ev){

          // Set return to Home value.
          SectionCollection.models[0].set('returnHome', true);

          var Router = require('router'),
          appRouter = new Router(scope.appsocket);
          appRouter.navigate('', {trigger:true });

          // Reset Panel model
          SectionCollection.models[0].set('subopen', false);
        },

        backClick:function(ev){

          var Router = require('router'),
          appRouter = new Router(scope.appsocket);
          appRouter.navigate('', {trigger:true });

          // Reset Panel model
          SectionCollection.models[0].set('subopen', false);
        },

        infoClick:function(ev){

        },


        loadPanelContent:function(panel){

          var Router = require('router'),
          appRouter = new Router();
          appRouter.navigate('panelContent', {trigger:true });
        },

        // Clean hanging events of the view on change :::::::::
        dispose:function(){
          scope.active = false;
        }

    });


    // Our module now returns our view
    return PanelUI;

});
