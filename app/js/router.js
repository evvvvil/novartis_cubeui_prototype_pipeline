
// ========
// ROUTER :
// ========

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES :::::::::::::::::::

    var $              = require('jquery'),
        Backbone       = require('backbone'),
        _              = require('underscore'),
        OuterView      = require('app/views/outer_ui'),
        Home           = require('view.intro'),
        PanelUI        = require('app/views/panelui'),
        // The Panel View
        PanelDetail      = require('app/views/panels/panel_detail');

    var scope,
        mainView = null,
        subView = null,
        homeView = null,
        contentView = null,
        container = '#container',
        inner = '#inner',
        innerUI = '#ui',
        innerContent = '#slide_viewport div', // appends inner div (drag component).
        el = '<div id="viewport"></div>',
        elUI = '<div id="viewportUI"></div>',
        elContent = '<div id="viewport_content"></div>';

   //define router class
    var AppRouter = Backbone.Router.extend ({
        routes: {
            '' : 'home',
            'home' : 'home',
            'panelui' : 'panelui',

            'panelContent' : 'panelContent',

        },

        initialize: function(){
               scope = this;
        },

        newView: function(View){

            if(mainView){
                mainView.unbind();
                mainView.remove();
            }

            mainView = new View();
        },

        homeView: function(View){

            if(!mainView){
                scope.newView( OuterView ); //Creates the main view if not there already
            }

            if(homeView){
              subView.dispose();
              subView.unbind();
              subView.remove();
              $(innerUI).append(elUI);
            }else{
              homeView = new View();
            }
        },

        subView: function(View){
            //
            // if(subView){
            //     subView.dispose();
            //     subView.unbind();
            //     subView.remove();
            //     $(innerUI).append(elUI);
            // }

            if(!mainView){
                scope.newView( OuterView ); //Creates the main view if not there already
            }

            // console.log('view is', subView);
            subView = new View();
        },

        contentView: function(View){

            if(contentView){
                contentView.dispose();
                contentView.unbind();
                contentView.remove();
                $(innerContent).append(elContent);
            }

            if(!subView){
              scope.subView( PanelUI ); //Creates the main view if not there already
            }

            // console.log('view is', subView);
            contentView = new View();
        },

        home: function () {
            //Pass in view to cleaner func
            scope.homeView( Home );
        },

        panelui: function () {
            scope.subView( PanelUI );
        },

        panelContent: function () {
          scope.contentView( PanelDetail );
        },
    });


  return AppRouter;

});
