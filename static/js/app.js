// code [ Josh : info@designbuildplay.co.uk ]
// ==============================
// THE MAIN APPLICATION LOGIC .
// ==============================

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                   = require('jquery'),
        Backbone            = require('backbone'),
        _                   = require('underscore'),
        Fastclick           = require('fastclick'),
        Router              = require('router');

    var initialize = function(){

        console.log('%c MCCGLC.com ', 'background: #f2ff1a; color: #0000; padding:2px; font-size:14px');
        console.log('%c CUBE INTERFACE ', 'background: #000000; color: #f2ff1a; padding:2px; font-size:14px');
        console.log('%c DEV Josh.Freeman ', 'background: #000000; color: #f2ff1a; padding:2px; font-size:14px');
        console.log('%c info@designbuildplay.co.uk ', 'background: #000000; color: #c3c3c3; padding:2px; font-size:14px');
        console.log('%c ', 'background: #ffffff; color: #ffffff; font-size:14px');

        var appRouter = new Router();  //define our new instance of router
        Backbone.history.start();   // use # History API

        //ADD FASTCLICK
        FastClick.attach(document.body);
  }

  return {
    initialize: initialize

  };

});
