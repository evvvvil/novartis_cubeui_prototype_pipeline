// THE INNER UI VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        TweenMax           = require('tweenmax'),
        SectionCollection  = require('app/collections/Projects'),
        template           = require("text!../../../templates/microui1.html");

    // CONTENT :::::::::::::::::::::::::::::::::::
    var scope;

    var HomeView = Backbone.View.extend({

        tagName:'div',
        id:"slides",
        el:'#viewport_content',  //selects element rendering to
        template: _.template( template ), //selects the template with given name

        events: {
          // 'click .videoslide': 'videoClick',
        },

        initialize:function () {
              scope = this;
              this.render();
              console.log("VIEWING microui 1");
        },

        render:function () {

            this.$el.html(this.template());

            TweenMax.to(scope.$el.find('.container'), 0, {alpha:1});

            return this;
        },


        // Clean hanging events of the view on change :::::::::::::::::::
        dispose:function(){

        }

    });


    // Our module now returns our view
    return HomeView;

});
