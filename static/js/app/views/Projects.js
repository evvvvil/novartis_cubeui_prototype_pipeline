define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),
        Project     = require('app/models/Project');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var PrjCollection = Backbone.Collection.extend({
      model:Project,
      url: '/'
    });

    var Navigation = new PrjCollection([
      {
        subopen:false,
        wallview:1,
        returnHome:false,
        wallcontent:{
          wall:1,
          cube:1,
        }
      }
    ]);

    // Return the model for the module
    return PrjCollection, Navigation;


});
