// THE INNER UI VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        TweenMax           = require('tweenmax'),
        Draggable          = require('Draggable'),
        Throw              = require('Throw'),
        SectionCollection  = require('app/collections/Projects'),
        template           = require("text!../../../../templates/panels/panel_5_1.html");

    // CONTENT :::::::::::::::::::::::::::::::::::
    var scope;

    var HomeView = Backbone.View.extend({

        tagName:'div',
        id:"slides",
        el:'#viewport_content',  //selects element rendering to
        template: _.template( template ), //selects the template with given name

        events: {
          'click .btn_info': 'infoClick',
          'click .btn_close': 'closeClick',
        },

        initialize:function () {
              scope = this;
              this.render();
              console.log("VIEWING panel detail");
        },

        render:function () {

            this.$el.html(this.template());

            TweenMax.to(scope.$el.find('.container'), 0, {alpha:1});

            // make content draggable
            this.contentscroll = Draggable.create(scope.$el.find('.scrollArea'), {
              type:"scrollTop", edgeResistance:0.96,
              throwProps:true, lockAxis:true,
            });

            return this;
        },

        infoClick:function() {
          console.log("showINFO");
          $(scope.$el.find('.btn_info')).css('display','none');
          $(scope.$el.find('.btn_close')).css('display','block');

          $(scope.$el.find('.info')).css('display','block');
          $(scope.$el.find('.scrollArea')).css('display','none');
        },

        closeClick:function() {
          console.log("showINFO");
          $(scope.$el.find('.btn_info')).css('display','block');
          $(scope.$el.find('.btn_close')).css('display','none');

          $(scope.$el.find('.info')).css('display','none');
          $(scope.$el.find('.scrollArea')).css('display','block');
        },

        // Clean hanging events of the view on change :::::::::::::::::::
        dispose:function(){

        }

    });


    // Our module now returns our view
    return HomeView;

});
