# Novartis_CubeUI prototype.

## 3D Cube UI.
BackboneJS app.

## To setup the project run:
$ npm install

It is using Compass to compile to SCSS,
so you may need to install this if you haven't got it running already (http://compass-style.org/install/)

## Then do within the directory:
$ compass watch

## To compile the project run
$ gulp

This will build and launch on localhost automatically as its using BrowserSync.
Browser sync will LiveReload the files, as setup in gulp, so if all installed it should then be good to go.

## Projects Structure
As mentioned before this is a BackboneJS app, using Require to make it modular.
Most of the ThreeJS setup take place in the home.js view.
There are 10 Cube stacks that make up the entire wall.
Each stack contains 12 cubes.
The positioning of the wall stacks can be seen in the pic below.
The camera is animated from one stack to the next depending on the choice.
All the information panels are loaded on a overlay in position 10.
The overlay info panels view and controlled in panelui.js and panel_details.js sub views.
The tweening I use Greensocks TweenMax, if you haven't used this before take a look (http://greensock.com/) its the boss at animating DOM and Canvas elements


## Content
I have added in all content for all the panels, as it currently stands. I did notice a couple didi not have content for the scroll area so this will need adding.
The content is made up of PNG's rather than html text, so it should be easy to update, just switch out the images.
The content images are in 'static/imgs/panel/' then the relevant folder, the number being of the WALL state and then the cube that was selected.. e.g 5_1
The content that is likely to change is just the scroll.png in each of the folders.
Cube face materials are in the cubes folder.

## Wall Layout.

[  2  ] [  3  ] [  5 ] [  6  ] [ 10  ]

[  1  ] [  4  ] [  7 ] [  8  ] [  9  ]
